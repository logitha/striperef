<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.daffodilsw.com/
 * @since      1.0.0
 *
 * @package    Wp_Custom_Donation
 * @subpackage Wp_Custom_Donation/public/partials
 */
//$publishable_key = $wpcd_publishable_key;
//$secret_key		 = $wpcd_secret_key;

$amount_value=number_format($amount,2);

?>
<section class="donation_checkout_form_section pading-bottom-30">
    <div class="donation-form-area">
        <div class="donation-form-container">  
            <div class="donation-form-row">
                <div class="form-box-size donation-form-3 box-shadow"> 
                    
                    <!--<div class="donation-form-title-3">
                        <h3>Stripe Payment Form</h3>
                    </div>-->  
                    <div class="donation-form-box-3">
                        <div class="form-wrapper"> 
                        <div id="wpcd-donation-payment-form-loader-info" class="wpcd-loader" style="display:none;">
                            <img src="<?php echo plugins_url('images/ajax-loader.gif', dirname(__FILE__)); ?>"/>
                            <span><?php _e('Please wait ...', $this->plugin_name); ?></span>
                        </div>
                        <div id="wpcd-donation-payment-form-alert" class="alert alert-danger" role="alert" style="display:none;"></div>
                        <form action="#" method="post" name="payment-form" id="payment-form">
                            <?php 
                            /*if($_GET['id']!=""){
                            ?>
                            <div class="form-group">
                                <div class="payment-success">Thanks for your payment. <br/> Your Transaction Id: <?php print $_GET['id']?></div>
                            </div>
                            <?php }*/ ?>
                            <input type="hidden" name="amount" id="amount" value="<?php echo $amount; ?>"  />
                            <input type="hidden" name="projectname" id="projectname" value="<?php echo $project_name; ?>"  />
                            <input type="hidden" name="projectid" id="projectid" value="<?php echo $project_id; ?>"  />
                            <input type="hidden" name="plantype" id="plantype" value="<?php echo $plan_type; ?>"  />
                            
                            <div class="form-group">
                                <label class="form-label" for="firstname">First Name</label>
                                <input name="firstname" id="firstname" class="form-input donate-cko-input" type="text"  />
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="lastname">Last Name</label>
                                <input name="lastname" id="lastname" class="form-input donate-cko-input" type="text"  />
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </div>
                            
                            <div class="form-group">
                                <label class="form-label" for="email">Email</label>
                                <input name="email" id="email" class="form-input donate-cko-input" type="email" />
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </div>
                            <div class="form-group2 card-details">
                                  <label class="form-label" for="password">Credit/Debit Card Details</label>
                                  <div id="card-element">
                                    
                                  </div> 
                                  <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>

                            <?php /* <div class="form-group">
                                <label class="form-label" for="cardnumber">Card Number</label>
                                <input name="cardnumber" id="cardnumber" class="form-input donate-cko-input" type="text" maxlength="16" data-stripe="number" />
                                <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                            </div>
                            <div class="form-group2">
                                  <label class="form-label" for="password">Expiry Month / Year & CVV</label>
                                  <i class="fa fa-calendar" aria-hidden="true"></i>
                            </div>
                            <div class="row">
                                <div class="form-group form-group2 col-12 col-md-4 col-lg-4">
                                    <select name="month" id="month" class="form-input2 donate-cko-input2" data-stripe="exp_month">
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="form-group form-group2 col-12 col-md-4 col-lg-4">
                                    <select name="year" id="year" class="form-input2 donate-cko-input2" data-stripe="exp_year">
                                        <option value="19">2019</option>
                                        <option value="20">2020</option>
                                        <option value="21">2021</option>
                                        <option value="22">2022</option>
                                        <option value="23">2023</option>
                                        <option value="24">2024</option>
                                        <option value="25">2025</option>
                                        <option value="26">2026</option>
                                        <option value="27">2027</option>
                                        <option value="28">2028</option>
                                        <option value="29">2029</option>
                                        <option value="30">2030</option>
                                    </select>
                                </div>
                                <div class="form-group form-group2 col-12 col-md-4 col-lg-4">
                                    <input name="cvv" id="cvv" class="form-input2 donate-cko-input2" type="text" placeholder="CVV" data-stripe="cvc" />  
                                </div>
                            </div> */ ?>


                            

                            <div class="form-group">
                                <div class="payment-errors" style="display:none;" id="payment-errors"></div>
                            </div>

                            <div class="button-style">
                                <button id="donation-pay-now-button" class="button login submit" >
                                    Pay now ($ <?php echo $amount_value; ?>) <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </button>
                            </div>
                            
                            </form>
                        </div>
                    </div>  
                    
                </div>
            </div> 
        </div> 
    </div> 
</section>
<?php

  
        ?>
     
 <!--cript  type="text/javascript">

  function button_onclick() {
 document.getElementById("donation-pay-now-button").style.visibility="hidden";
}    
</script-->
     
<script id="stripe-js" type="text/javascript">
/**/
        var stripe = Stripe(stripe_publishable_key);
        var elements = stripe.elements();
        
        // Custom styling can be passed to options when creating an Element.
        var style = {
        base: {
            // Add your base input styles here. For example:
            fontSize: '16px',
            color: '#32325d',
        },
        };
        // Create an instance of the card Element.
        var card = elements.create('card', {hidePostalCode: true,style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        
        (function($) {
    'use strict';
        card.on('change', function(event) {
            var errorElement = $('#payment-errors');
          if (event.error) {
            errorElement.show();
            errorElement.html(event.error.message);
          } else {
            errorElement.hide();
          }
        });
    })(jQuery);

</script> 

       
<?php /*<script type="text/javascript">
	Stripe.setPublishableKey('<?php print $publishable_key; ?>');
    var $form = $('#payment-form');
    $form.submit(function(event) {
        // Disable the submit button to prevent repeated clicks:
        $form.find('.submit').prop('disabled', true);
    
        // Request a token from Stripe:
        Stripe.card.createToken($form, stripeResponseHandler);
    
        // Prevent the form from being submitted:
        return false;
    }); 
    function stripeResponseHandler(status, response) {
	  // Grab the form:
	  var $form = $('#payment-form');
	
	  if (response.error) { // Problem!
	
		// Show the errors on the form:
		$form.find('.payment-errors').text(response.error.message);
		$form.find('.submit').prop('disabled', false); // Re-enable submission
	
	  } else { // Token was created!
	
		// Get the token ID:
		var token = response.id;

		// Insert the token ID into the form so it gets submitted to the server:
		$form.append($('<input type="hidden" name="stripeToken">').val(token));
	
		// Submit the form:
		$form.get(0).submit();
	  }
	} 
  
    </script> */ ?>

    
	

    <?php
?>

