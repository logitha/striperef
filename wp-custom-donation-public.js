(function($) {
    'use strict';

    $(document).ready(initScript);

    function initScript() {

        //defing global ajax post url
        window.ajaxPostUrl = ajax_object.ajax_url;
        // validating login form request
        wpcdValidateAndProcessLoginForm();
        // validating registration form request
        wpcdValidateAndProcessRegisterForm();
        // validating reset password form request
        wpcdValidateAndProcessResetPasswordForm();
        //Show Reset password
        wpcdShowResetPasswordForm();
        //Return to login
        wpcdReturnToLoginForm();
        generateCaptcha();

        wpcdValidateDonationPaymentForm();
        wpcdShowDonateButtonProcess();


    }

    // Validate login form
    function wpcdValidateAndProcessLoginForm() {
        $('#wpcdLoginForm').formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                wpcd_donation_username: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The username is required.'
                        }
                    }
                },
                wpcd_donation_password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required.'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $('#wpcd-donation-login-alert').hide();
            // You can get the form instance
            var $loginForm = $(e.target);
            // and the FormValidation instance
            var fv = $loginForm.data('formValidation');
            var content = $loginForm.serialize();

            // start processing
            $('#wpcd-donation-login-loader-info').show();
            wpcdStartLoginProcess(content);
            // Prevent form submission
            e.preventDefault();
        });
    }

    // Make ajax request with user credentials
    function wpcdStartLoginProcess(content) {

        var loginRequest = jQuery.ajax({
            type: 'POST',
            url: ajaxPostUrl,
            data: content + '&action=wpcd_donation_user_login',
            dataType: 'json',
            success: function(data) {
                $('#wpcd-donation-login-loader-info').hide();
                // check login status
                if (true == data.logged_in) {
                    $('#wpcd-donation-login-alert').removeClass('alert-danger');
                    $('#wpcd-donation-login-alert').addClass('alert-success');
                    $('#wpcd-donation-login-alert').show();
                    $('#wpcd-donation-login-alert').html(data.success);

                    // redirect to redirection url provided
                    window.location = data.redirection_url;

                } else {

                    $('#wpcd-donation-login-alert').show();
                    $('#wpcd-donation-login-alert').html(data.error);

                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    // Validate registration form


    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    function generateCaptcha() {
        $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));
    }

    // Validate registration form
    function wpcdValidateAndProcessRegisterForm() {
        $('#wpcdRegisterForm').formValidation({
            message: 'This value is not valid',
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                wpcd_donation_fname: {
                    validators: {
                        notEmpty: {
                            message: 'The first name is required'
                        },
                        stringLength: {
                            max: 30,
                            message: 'The firstname must be less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z_ ]*$/,
                            message: 'Only characters are allowed.'
                        }
                    }
                },
                wpcd_donation_username: {
                    message: 'The username is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The username is required'
                        },
                        stringLength: {
                            min: 6,
                            max: 30,
                            message: 'The username must be more than 6 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z0-9_\.]+$/,
                            message: 'The username can only consist of alphabetical, number, dot and underscore'
                        }
                    }
                },
                wpcd_donation_email: {
                    validators: {
                        notEmpty: {
                            message: 'The email is required'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                wpcd_donation_password: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                },
                wpcd_donation_password2: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        identical: {
                            field: 'wpcd_donation_password',
                            message: 'The password and its confirm are not the same'
                        },
                        stringLength: {
                            min: 6,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                },
                wpcd_donation_accept_disclaimer: {
                    validators: {
                        notEmpty: {
                            message: 'Accept the terms and conditions'
                        },
                    }
                },
                wpcd_donation_captcha: {
                    validators: {
                        callback: {
                            message: 'Wrong answer',
                            callback: function(value, validator, $field) {
                                var items = $('#captchaOperation').html().split(' '),
                                        sum = parseInt(items[0]) + parseInt(items[2]);
                                return value == sum;
                            }
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $('#wpcd-donation-register-alert').hide();
            $('#wpcd-donation-mail-alert').hide();
            $('body, html').animate({
                scrollTop: 0
            }, 'slow');
            // You can get the form instance
            var $registerForm = $(e.target);
            // and the FormValidation instance
            var fv = $registerForm.data('formValidation');
            var content = $registerForm.serialize();

            // start processing
            $('#wpcd-donation-reg-loader-info').show();
            wpcdStartRegistrationProcess(content);
            // Prevent form submission
            e.preventDefault();
        }).on('err.form.fv', function(e) {
            // Regenerate the captcha
            generateCaptcha();
        });
    }


    // Make ajax request with user credentials
    function wpcdStartRegistrationProcess(content) {

        var registerRequest = $.ajax({
            type: 'POST',
            url: ajaxPostUrl,
            data: content + '&action=wpcd_donation_user_registration',
            dataType: 'json',
            success: function(data) {

                $('#wpcd-donation-reg-loader-info').hide();
                //check mail sent status
                if (data.mail_status == false) {

                    $('#wpcd-donation-mail-alert').show();
                    $('#wpcd-donation-mail-alert').html('Could not able to send the email notification.');
                }
                // check login status
                if (true == data.reg_status) {
                    $('#wpcd-donation-register-alert').removeClass('alert-danger');
                    $('#wpcd-donation-register-alert').addClass('alert-success');
                    $('#wpcd-donation-register-alert').show();
                    $('#wpcd-donation-register-alert').html(data.success);

                } else {
                    $('#wpcd-donation-register-alert').addClass('alert-danger');
                    $('#wpcd-donation-register-alert').show();
                    $('#wpcd-donation-register-alert').html(data.error);

                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    function wpcdShowResetPasswordForm() {
        $('#btnForgotPassword').click(function() {
              $('#wpcdResetPasswordSection').removeClass('hidden');
              $('#wpcdLoginForm').slideUp(500);  
               $('#wpcdResetPasswordSection').slideDown(500);
        });
    }
    
    function wpcdReturnToLoginForm() {
        $('#btnReturnToLogin').click(function() {
              $('#wpcdResetPasswordSection').slideUp(500);              
              $('#wpcdResetPasswordSection').addClass('hidden');
              $('#wpcdLoginForm').removeClass('hidden');
              $('#wpcdLoginForm').slideDown(500);               
        });
    }

    // Validate reset password form
    //Neelkanth
    function wpcdValidateAndProcessResetPasswordForm() {

        $('#wpcdResetPasswordForm').formValidation({
            message: 'This value is not valid',
            icon: {
                required: 'glyphicon glyphicon-asterisk',
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                wpcd_donation_rp_email: {
                    validators: {
                        notEmpty: {
                            message: 'Please enter your email address which you used during registration.'
                        },
                        regexp: {
                            regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                wpcd_donation_newpassword: {
                    validators: {
                        notEmpty: {
                            message: 'The password is required'
                        },
                        stringLength: {
                            min: 6,
                            message: 'The password must be more than 6 characters long'
                        }
                    }
                }
            }
        }).on('success.form.fv', function(e) {
            $('#wpcd-donation-resetpassword-alert').hide();

            $('body, html').animate({
                scrollTop: 0
            }, 'slow');
            // You can get the form instance
            var $resetPasswordForm = $(e.target);
            // and the FormValidation instance
            var fv = $resetPasswordForm.data('formValidation');
            var content = $resetPasswordForm.serialize();
            
            // start processing
            $('#wpcd-donation-resetpassword-loader-info').show();
            wpcdStartResetPasswordProcess(content);
            // Prevent form submission
            e.preventDefault();
        });
    }

    // Make ajax request with email
    //Neelkanth
    function wpcdStartResetPasswordProcess(content) {
        
        var resetPasswordRequest = jQuery.ajax({
            type: 'POST',
            url: ajaxPostUrl,
            data: content + '&action=wpcd_donation_resetpassword',
            dataType: 'json',
            success: function(data) {
                
                $('#wpcd-donation-resetpassword-loader-info').hide();
                // check login status
                if (data.success) {
                    
                    $('#wpcd-donation-resetpassword-alert').removeClass('alert-danger');
                    $('#wpcd-donation-resetpassword-alert').addClass('alert-success');
                    $('#wpcd-donation-resetpassword-alert').show();
                    $('#wpcd-donation-resetpassword-alert').html(data.success);

                } else {

                    $('#wpcd-donation-resetpassword-alert').show();
                    $('#wpcd-donation-resetpassword-alert').html(data.error);

                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    }

    function wpcdShowDonateButtonProcess(){
        $('#donation-form').on('click','#donation-pay-button',function(e){
            e.preventDefault;   
            var form = $('#donation-form')[0]; // You need to use standard javascript object here
            var formData = new FormData(form);   
            // our AJAX identifier
            formData.append('action', 'wpcd_donation_form_actions'); 
            jQuery.ajax({
                type: 'POST',
                url: ajaxPostUrl,
                data: formData,
                contentType: false,
                processData: false,
                success: function(data) {

                    if(data.error) {
                    
                        $('#wpcd-payment-btn-alert').removeClass('alert-success');
                        $('#wpcd-payment-btn-alert').addClass('alert-danger');
                        $('#wpcd-payment-btn-alert').show();
                        $('#wpcd-payment-btn-alert').html(data.message);
    
                    } else {
    
                        $('#wpcd-payment-btn-alert').hide();
                        $('#wpcd_donation_checkout_popup_form').modal('show');//now its working
                        $('#wpcd_donation_checkout_popup_form .modal-body').html(data)
    
                    }
                    
                    
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });

            $('body').on('focus','.donate-cko-input',function(){
              $(this).parents('.form-group').addClass('focused');
            });
            $('body').on('blur','.donate-cko-input',function(){
              var inputValue = $(this).val();
              if ( inputValue == "" ) {
                $(this).removeClass('filled');
                $(this).parents('.form-group').removeClass('focused');  
              } else {
                $(this).addClass('filled');
              }
            });  
    }
    // Validate reset password form
    //Neelkanth
    function wpcdValidateDonationPaymentForm() {
        var countform = 0;
        $('body').on('click','#donation-pay-now-button',function(){
            $('#payment-form').formValidation({
                message: 'This value is not valid',
                icon: {
                    required: 'glyphicon glyphicon-asterisk',
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    firstname: {
                        validators: {
                            notEmpty: {
                                message: 'The first name is required'
                            },
                            stringLength: {
                                max: 30,
                                message: 'The first name must be less than 30 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z_ ]*$/,
                                message: 'Only characters are allowed.'
                            }
                        }
                    },
                    lastname: {
                        message: 'The last name is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The last name is required'
                            },
                            stringLength: {
                                max: 30,
                                message: 'The last name must be less than 30 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z_ ]*$/,
                                message: 'Only characters are allowed.'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'The email is required'
                            },
                            regexp: {
                                regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                                message: 'The value is not a valid email address'
                            }
                        }
                    },
                    cardnumber: {
                        validators: {
                            notEmpty: {
                                message: 'The cardnumber is required'
                            },
                            stringLength: {
                                min: 16,
                                max: 16,
                                message: 'The cardnumber must 16 numbers'
                            },
                        }
    
                    },
                    year: {
                        validators: {
                            notEmpty: {
                                message: 'The year is required'
                            }
                        }
    
                    },
                    month: {
                        validators: {
                            notEmpty: {
                                message: 'The month is required'
                            }
                        }
    
                    },
                    cvv: {
                        validators: {
                            notEmpty: {
                                message: 'The cvv is required'
                            }
                        },
                        stringLength: {
                            min: 3,
                            max: 3,
                            message: 'The cvv must 3 numbers'
                        }
    
                    }
                }
            }).on('success.form.fv', function(e) {
                
                //wpcdStartResetPasswordProcess(content);
                // Prevent form submission
                e.preventDefault();
                // Set your publishable key: remember to change this to your live publishable key in production
                // See your keys here: https://dashboard.stripe.com/account/apikeys
                var form = $('#payment-form');
                var errorElement = $('#payment-errors');
                //var stripe = Stripe(stripe_publishable_key);

                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                       // console.log(result.error);
                      // Inform the customer that there was an error.
                      errorElement.show();
                      errorElement.html(result.error.message);
                    } else {
                        var counform = countform++;
                      // Send the token to your server.
                      errorElement.hide();
                        $('#wpcd-donation-payment-form-alert').hide();
                        // You can get the form instance
                        var $donationCheckoutForm = $(e.target);
                        // and the FormValidation instance
                        var fv = $donationCheckoutForm.data('formValidation');
                        var content = $donationCheckoutForm.serialize();
                
                        // start processing
                        $('#wpcd-donation-payment-form-loader-info').show();

                        var token=result.token;

                        if(token.id){
                            form.append($('<input type="hidden" name="stripeToken">').val(token.id));
                            var formData = new FormData(form[0]);
                             if(counform == 0) {
                            stripeDonationPaymentProcess(formData)
                             }
                        }
                        
                    }
                  });
                
                 
                /*$form.submit(function(event) {
                    // Disable the submit button to prevent repeated clicks:
                    $form.find('.submit').prop('disabled', true);
                
                    // Request a token from Stripe:
                    Stripe.card.createToken($form, stripeResponseHandler);
                
                    // Prevent the form from being submitted:
                    return false;
                }); */       
                
            });

        });
        

       
    }
    function stripeResponseHandler() {
        // Grab the form:
        var $form = $('#payment-form');
        var errorElement = $('#payment-errors');
  
        cardElement.on('change', function(event) {
          if (event.error) {
            errorElement.show();
            errorElement.html(result.error.message);
          } else {
            errorElement.hide();
          }
        });
      
       
    }
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = $('#payment-form');


        var formData = new FormData(form[0]);  
        stripeDonationPaymentProcess(formData) 
       
        
        /*var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput); */  
            
            // our AJAX identifier
             
      
        // Submit the form
        //form.submit();
    }
    function stripeDonationPaymentProcess(formData){

        formData.append('action', 'wpcd_donation_payment_process');
        jQuery.ajax({
            type: 'POST',
            url: ajaxPostUrl,
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
                // end processing
                $('#wpcd-donation-payment-form-loader-info').hide();
                if (data.success) {
                    
                    $('#wpcrl-donation-payment-form-alert').removeClass('alert-danger');
                    $('#wpcrl-donation-payment-form-alert').addClass('alert-success');
                    $('#wpcrl-donation-payment-form-alert').show();
                    $('#wpcrl-donation-payment-form-alert').html(data.message);

                    $('#wpcd-payment-btn-alert').removeClass('alert-danger');
                    $('#wpcd-payment-btn-alert').addClass('alert-success');
                    $('#wpcd-payment-btn-alert').show();
                    $('#wpcd-payment-btn-alert').html(data.message);

                    $('#wpcd_donation_checkout_popup_form').modal('hide');
                    $('#payment-form').remove();
                    

                } else if (data.error) {
                    $('#wpcd-donation-payment-form-alert').removeClass('alert-success');
                    $('#wpcd-donation-payment-form-alert').addClass('alert-danger');
                    $('#wpcd-donation-payment-form-alert').show();
                    $('#wpcd-donation-payment-form-alert').html(data.message);

                }
            },
            error: function(data) {
                console.log(data);
            }
            
        });
        

    }
    
/*
check_submit();
function check_submit() {
 document.getElementById("donation-pay-now-button").style.visibility="hidden";
}    */


})(jQuery);
