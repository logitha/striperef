<?php
/**
 * The public-facing functionality of the plugin.
 *
 *
 * @package    Wp_Custom_Donation
 * @subpackage Wp_Custom_Donation/public
 * @author     Jenis Patel <jenis.patel@daffodilsw.com>
 */
require_once 'class-wp-custom-donation-generic-public.php';

class Wp_Custom_Donation_Public extends Wp_Custom_Donation_Generic_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/wp-custom-donation-public.css', array(), $this->version, 'all');
        //wp_enqueue_style($this->plugin_name . '-bootstrap', plugin_dir_url(__FILE__) . 'css/bootstrap.min.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name . '-formValidation', plugin_dir_url(__FILE__) . 'css/formValidation.min.css', array(), $this->version, 'all');
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/wp-custom-donation-public.js', array('jquery'), $this->version, false);
        //wp_enqueue_script($this->plugin_name . '-bootstrap', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-formValidation.min', plugin_dir_url(__FILE__) . 'js/validator/formValidation.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-stripe-v2', 'https://js.stripe.com/v2/', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name . '-stripe-v3', 'https://js.stripe.com/v3/', array('jquery'), $this->version, false);        
        wp_enqueue_script($this->plugin_name . '-bootstrap-validator', plugin_dir_url(__FILE__) . 'js/validator/bootstrap-validator.min.js', array('jquery'), $this->version, false);

        $wpcd_donation_settings = get_option('wpcd_stripepayment_settings');
        $wpcd_publishable_key="";
        $wpcd_secret_key	= "";
        if(!empty($wpcd_donation_settings)){
            $payment_mode = (empty($wpcd_donation_settings['wpcd_payment_mode']) || $wpcd_donation_settings['wpcd_payment_mode'] == '') ? '' : $wpcd_donation_settings['wpcd_payment_mode'];
            
            if($payment_mode=='sandboxmode'){
                $wpcd_publishable_key = $wpcd_donation_settings['wpcd_test_publishable_key'];
                $wpcd_secret_key		 = $wpcd_donation_settings['wpcd_test_secret_key'];
            }else if($payment_mode=='livemode'){
                $wpcd_publishable_key = $wpcd_donation_settings['wpcd_live_publishable_key'];
                $wpcd_secret_key = $wpcd_donation_settings['wpcd_live_secret_key'];
            }
        }
        

        wp_localize_script($this->plugin_name, "stripe_publishable_key",  $wpcd_publishable_key);
        //wp_localize_script($this->plugin_name, "stripe_secret_key",  $wpcd_secret_key);
        // localizing gloabl js objects
        wp_localize_script($this->plugin_name, 'ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
    }

    /**
     * Render the login form
     *
     * @since   1.0.0
     */
    public function wpcd_donation_display_login_form()
    { 
                
          
        // Buffer output
        ob_start();
        include_once 'partials/wpcd-donation-form.php';
        // Return buffer
        return ob_get_clean();
    }

   
    public function wpcd_donation_success_mail($infodate)
    {
        $wpcd_donation_email_settings = get_option('wpcd_email_settings');

       

        //prepare placeholder array

        $placeholders = array(
            '%TRANSACTIONID%' => $infodate['transcation_id'],
            '%PAYMENTID%' => $infodate['payment_id'],
            '%CUSTOMERFULLNAME%' => $infodate['customer_fullname'],
            '%CUSTOMERFIRSTNAME%' => $infodate['customer_fname'],
            '%CUSTOMERLASTNAME%' => $infodate['customer_lname'],
            '%CUSTOMEREMAIL%' => $infodate['customer_email'],
            '%DONATECATEGORY%' => $infodate['donation_project'],
            '%DONATEPLAN%' => $infodate['donation_plan'],
            '%DONATEAMOUNT%' => "$ ".number_format($infodate['donation_amount'],2), 
            '%BLOGNAME%' => get_option('blogname'),           
            '%BLOGURL%' => "<a href='" . site_url() . "'>" . site_url() . "</a>",
        );      


        $to['admin'] = get_option('admin_email');
       

        $subject['admin'] = 'Donation received from ' . get_option('blogname');

        

        //Subject when settngs are saved
        if (!empty($wpcd_donation_email_settings['wpcd_donation_success_subject'])) {
            $subject['user'] = $wpcd_donation_email_settings['wpcd_donation_success_subject'];
            //$subject['user'] = str_replace('%BLOGNAME%', get_option('blogname'), $subject['user']);
            $subject['user'] = Wp_Custom_Donation_Generic_Public::generic_placeholder_replacer($subject['user'], $placeholders);
        }

        // using content type html for emails
        $headers = array();
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        $headers[] = 'From:' . html_entity_decode(get_bloginfo('name')) . ' <' . get_bloginfo('admin_email') . '>';
        //$headers[] = 'Cc: nanbanfoundation2020@gmail.com'; 
         
        
        //Make body of admin message

        $userprofile = '<br><br><br><b>Payment received details</b><br><br>';
        $userprofile.= '<strong>First Name :</strong>'.$infodate['customer_fname'].'<br>';
        $userprofile.= '<strong>Last Name : </strong>'.$infodate['customer_lname'].'<br>';
        $userprofile.= '<strong>Email :</strong> '.$infodate['customer_email'].'<br>';
        $userprofile.= '<br><br>';
        $userprofile.= '<strong>Payment ID :</strong>'.$infodate['payment_id'].'<br>';
        $userprofile.= '<strong>Transaction ID :</strong>'.$infodate['transcation_id'].'<br>';
        $userprofile.= '<strong>Donation Amount :</strong> '."$ ".number_format($infodate['donation_amount'],2).'<br>';
        $userprofile.= '<strong>Donation Category :</strong>'.$infodate['donation_project'].'<br>';
        $userprofile.= '<strong>Payment Plan  :</strong> '.$infodate['donation_plan'].'<br>';

        $message['admin'] = sprintf(__('Donation has received from <b>%s</b> for <b>%s</b> with following details:'), $infodate['customer_fullname'],$infodate['donation_project']);

        

        $footer['admin'] = '<br><br>' . __('Thanks.');

        $body['admin'] = $message['admin'] . $userprofile;

       /* $message=$message['admin'] . $userprofile . $footer['admin'];;
        ob_start();
        include 'email-templates/notification-email.php';
        $body['admin']  = ob_get_contents();
        ob_end_clean(); */

        

        //sending email notification to admin
        if (!empty($wpcd_donation_email_settings['wpcd_donation_admin_email_notification'])) {
            wp_mail($to['admin'], $subject['admin'], $body['admin'], $headers);
        }

        $to['user'] = $infodate['customer_email'];
        //Default subject
        $subject['user'] = 'Thanks for Donation';

        $user_header = array();
        $user_header[] = 'Content-Type: text/html; charset=UTF-8';
        $user_header[] = 'From:' . html_entity_decode(get_bloginfo('name')) . ' <' . get_bloginfo('admin_email') . '>';


        //Email when settings are saved
        //if (!empty($wpcd_donation_email_settings['wpcd_donation_success_message'])) {
            $message['user'] = $wpcd_donation_email_settings['wpcd_donation_success_message'];

            $message['user'] = Wp_Custom_Donation_Generic_Public::generic_placeholder_replacer($message['user'], $placeholders);
        //}
        $message=$message['user'];
        ob_start();
        include 'email-templates/notification-email.php';
        $body['user'] = ob_get_contents();
        ob_end_clean();
        //sending email notification to user
       $status = wp_mail($to['user'], $subject['user'], $body['user'], $user_header);
       //$status = "To:".$to['user']." sub:".$subject['user']." msg:".$body['user']." head:".json_encode($headers);
        return $status;
    }


    /* Donation Button */
    function wpcd_donation_display_button($atts){
        // Buffer output
       ob_start();
        
       $attributes = shortcode_atts( array(
           'open_form' => "replace",
           'button_label' => "Payment Information",
           'paid_amount' => '',
           'paid_type' => '',
           'post_id' => '',
           ), $atts );
           $error=array();
           if($attributes['paid_amount']=="" || $attributes['paid_amount']==0){
            $error[]="Paid Amount is Empty. Please Fill atleast $1.00";
           }
           if($attributes['paid_type']==""){
            $error[]="Please select payment method";
           }
           if($attributes['post_id']==""){
            $error[]="Please select payment category";
           }
           if(empty($error)){
                $placeholders = array(
                    '%PAYMOUNT%' => "$ ".$attributes['paid_amount']
                );
                $label=Wp_Custom_Donation_Generic_Public::generic_placeholder_replacer($attributes['button_label'], $placeholders);
                $wpcd_donation_settings = get_option('wpcd_stripepayment_settings');
                $payment_mode = (empty($wpcd_donation_settings['wpcd_payment_mode']) || $wpcd_donation_settings['wpcd_payment_mode'] == '') ? '' : $wpcd_donation_settings['wpcd_payment_mode'];
                
                $wpcd_publishable_key="";
                $wpcd_secret_key	= "";
                if($payment_mode=='sandboxmode'){
                    $wpcd_publishable_key = $wpcd_donation_settings['wpcd_test_publishable_key'];
                    $wpcd_secret_key		 = $wpcd_donation_settings['wpcd_test_secret_key'];
                }else if($payment_mode=='livemode'){
                    $wpcd_publishable_key = $wpcd_donation_settings['wpcd_live_publishable_key'];
                    $wpcd_secret_key = $wpcd_donation_settings['wpcd_live_secret_key'];
                }
                ?>
                    
                   
                    <?php if($wpcd_publishable_key=="" || $wpcd_secret_key=="" ){ ?>
                        <div id="wpcd-payment-key-status" class="wpcd-payment-key-status error-message">Stripe API Key's was missing</div>
                    <?php }else{ ?>
                    <a class="payment-btn donation-pay-button" id="donation-pay-button" dataprojectid="<?php echo $attributes['post_id']; ?>" dataproductprice="<?php echo $attributes['paid_amount']; ?>" href="javascript:void(0)"><?php echo $label; ?></a>  
                    <div id="wpcd-payment-btn-alert" class="wpcd-payment-btn-alert alert alert-message" style="display:none;"></div>
                    <?php } ?>
                     
                <?php
           }else{
               echo implode('<br/>',$error);
           }

        // Return buffer
        return ob_get_clean();
    }
    public function wpcd_donation_display_checkout_form($atts)
    {
        $attributes = shortcode_atts( array(
            'project_name' => "",
            'project_id' => "",
            'amount' => '',
            'plantype' => ''
            ), $atts );
            
          
        // Buffer output
        ob_start();
        $wpcd_donation_settings = get_option('wpcd_stripepayment_settings');
        $payment_mode = (empty($wpcd_donation_settings['wpcd_payment_mode']) || $wpcd_donation_settings['wpcd_payment_mode'] == '') ? '' : $wpcd_donation_settings['wpcd_payment_mode'];
        
        $wpcd_publishable_key="";
        $wpcd_secret_key	= "";
        if($payment_mode=='sandboxmode'){
            $wpcd_publishable_key = $wpcd_donation_settings['wpcd_test_publishable_key'];
            $wpcd_secret_key		 = $wpcd_donation_settings['wpcd_test_secret_key'];
        }else if($payment_mode=='livemode'){
            $wpcd_publishable_key = $wpcd_donation_settings['wpcd_live_publishable_key'];
            $wpcd_secret_key = $wpcd_donation_settings['wpcd_live_secret_key'];
        } 

        $plan_type=$attributes['plantype'];
        $project_id=$attributes['project_id'];
        $amount=$attributes['amount'];
        $project_name=($project_id!="") ? get_the_title( $project_id ) : ""; 
        $project_name=($attributes['project_name']!="") ? $attributes['project_name'] : $project_name;
        include_once 'partials/wpcd-donation-form.php';

        // Return buffer        
        return ob_get_clean();
    }

    function wpcd_donation_form_actions(){

        $plan_type=$_POST['payment-time'];
        $project_id=$_POST['select-charitable-projects'];
        $amount=($_POST['donate-amount']=="other") ? $_POST['other-amount'] : $_POST['donate-amount']; 
        $response=array();      
        if($amount=="" || $amount==0){
            $response['error']=true;
            $response['message']="Please enter amount value";
            
        }
        if($project_id=="" || $project_id==0){
            $response['error']=true;
            $response['message']="Please select any project";

        }
        if($plan_type==""){
            $response['error']=true;
            $response['message']="Please select any plan";
            
        }

        if(!empty($response)){
            wp_send_json($response);
        }else{
            echo do_shortcode("[wpcd_donation_checkout_form plantype='".$plan_type."' project_id='".$project_id."' amount='".$amount."']");
        }

       

        die();
        
    }
    function wpcd_donation_payment_process(){
        global $wpdb;

        $customer_table_name = $wpdb->prefix . 'donation_customer_list';
        $product_table_name = $wpdb->prefix . 'donation_product_list';
        $product_price_table_name = $wpdb->prefix . 'donation_product_price_list';
        $subscription_table_name = $wpdb->prefix . 'donation_subscriptions_list';
        $order_table_name = $wpdb->prefix . 'donation_orders_list';
        $response=array();
        $wpcd_donation_settings = get_option('wpcd_stripepayment_settings');
        $payment_mode = (empty($wpcd_donation_settings['wpcd_payment_mode']) || $wpcd_donation_settings['wpcd_payment_mode'] == '') ? '' : $wpcd_donation_settings['wpcd_payment_mode'];
        
        $wpcd_publishable_key="";
        $wpcd_secret_key	= "";
        if($payment_mode=='sandboxmode'){
            $wpcd_publishable_key = $wpcd_donation_settings['wpcd_test_publishable_key'];
            $wpcd_secret_key		 = $wpcd_donation_settings['wpcd_test_secret_key'];
        }else if($payment_mode=='livemode'){
            $wpcd_publishable_key = $wpcd_donation_settings['wpcd_live_publishable_key'];
            $wpcd_secret_key = $wpcd_donation_settings['wpcd_live_secret_key'];
        }
       
        if(isset($_POST['stripeToken'])){
            $stripe= \Stripe\Stripe::setApiKey($wpcd_secret_key);
            $description 	= $_POST['projectname'];
            $planName       = $_POST['projectname'];
            $post_id       = $_POST['projectid'];
            $amount_cents 	= $_POST['amount'] * 100;
            $tokenid		= $_POST['stripeToken'];
            $email  = strip_tags(trim($_POST['email']));


            $new_customer="create";
            $new_product = "create";
            $new_plan ="create";
           
                 
        
            // User Info
            $name_first = $_POST['firstname'];
            $name_last = $_POST['lastname'];
            $full_name = $name_first." ".$name_last;
            $user_info = array("first_name" => $name_first, "last_name" => $name_last,"name" => $full_name,"created_by"=> "Nanbanfoundation Donation");
        
            if($wpdb->get_var( "show tables like '$customer_table_name'" ) ==  $customer_table_name) 
            {
                $strip_cust_args=array(
                    "email_id" =>  $email,
                    "stripe_mode" =>  $payment_mode,
                );
                $stripe_customer_list=$this->get_stripe_customer_list($strip_cust_args);
                
                if(!empty($stripe_customer_list)){
                    try{
                        $customer = \Stripe\Customer::retrieve(
                            $stripe_customer_list[0]->stripe_customer_id
                        );
                        $customer_user_id=$stripe_customer_list[0]->id;
                    }catch (invalid_request_error $e) {
                        // Invalid parameters were supplied to Stripe's API
                        $customer = "";
                        $response['message'] = $e->getMessage();
                    }
                    if(property_exists($customer, 'deleted')){
                        if($customer->deleted==1)
                            $new_customer="update";
                    }else{
                        $new_customer="avaliable";
                    }
                }
                if($new_customer!="avaliable"){
                    try{
                        $customer = \Stripe\Customer::create(array(
                            'email' => $email, 
                            'source' => $tokenid,
                            "metadata" => $user_info,
                            "name"=> $full_name )
                        );
                        $customer_id=$customer->id;
                        if($new_customer=="update"){
                            $data_update =  array( 
                                'email_id' => $email, 
                                'stripe_customer_id' => $customer_id, 
                                'f_name' => $name_first,
                                'l_name' => $name_last,
                                'stripe_mode' =>  $payment_mode,
                            );
                            $data_where = array('id' => $stripe_customer_list[0]->id);
                            $wpdb->update($customer_table_name , $data_update, $data_where);
                        }else{
                            $wpdb->insert( 
                                $customer_table_name, 
                                array( 
                                    'email_id' => $email, 
                                    'stripe_customer_id' => $customer_id, 
                                    'f_name' => $name_first,
                                    'l_name' => $name_last,
                                    'stripe_mode' =>  $payment_mode,
                                ) 
                            );
                            $customer_user_id =$wpdb->insert_id;
                        } 
                        $new_customer="avaliable";                      
                        
                
                    }catch(Exception $e) {			
                        $response['error'] = true;
                        $response['message'] = $e->getMessage();
                        $result = "declined customer";
                    }
                }
            }
        
            if($_POST['plantype']=="onetime" || $_POST['plantype']=="one-time"){
                if($new_customer=="avaliable"){
                    try {			
            
                        $charge = \Stripe\Charge::create(array( 
                            "amount" 		=> $amount_cents,
                            "currency" 		=> "usd",
                            "customer"      => $customer->id,
                            "description" 	=> $description )			  
                        );
                        
                        $id			= $charge->id;
                        $amount 	= $charge->amount;
                        $currency 	= $charge->currency;
                        $status 	= $charge->status;
                        $created 	= $charge->created;
                        $result = "succeeded";

                        $wpdb->insert( 
                            $order_table_name, 
                            array( 
                                'user_id' => $customer_user_id,
                                'txn_id' => $id, 
                                'paid_amount' => ($amount/100), 
                                'paid_amount_currency' => $currency,
                                'project_name' => $description,
                                'payment_type' => $_POST['plantype'],
                                'create_date' => date('Y-m-d H:i:s', $created),                                
                                'payment_status' => $status,
                                'stripe_mode' =>  $payment_mode,
                            ) 
                        );

                        $lastid = $wpdb->insert_id;

                        $mail_info_data=array(
                            'transcation_id' => $id,
                            'payment_id' => $lastid,
                            'customer_fullname' => $full_name,
                            'customer_fname' => $name_first,
                            'customer_lname' => $name_last,
                            'customer_email' => $email,
                            'donation_project' => $planName,
                            'donation_amount' => ($amount/100),
                            'paid_amount_currency' => $currency,
                            'donation_plan' => $_POST['plantype'],

                        );
                        $response['mail_status'] = $this->wpcd_donation_success_mail($mail_info_data);
                        
                        $response['success'] = true;
                        //$response['message'] = "Thanks for your payment. <br/> Your Transaction Id: ".$id;
                        $response['message'] = "Thanks for your payment.";
                    
            
                    }catch(Exception $e) {			
                        $response['error'] = true;
                        $response['message'] = $e->getMessage();
                        $result = "declined";
                    }
                }
            }else if($_POST['plantype']=="monthly"){
                if($new_customer=="avaliable"){
                    // Create a product
                    $strip_prod_args=array(
                        "post_id" =>  $post_id,
                        "stripe_mode" =>  $payment_mode,
                    );
                    $stripe_product_list=$this->get_stripe_product_list($strip_prod_args);
                    if(!empty($stripe_product_list)){
                        try{
                            $product = \Stripe\Product::retrieve(
                                $stripe_product_list[0]->stripe_product_id
                            );
                            $stripe_product_id	= $product->id;
                        }catch (Exception $e) {
                            // Invalid parameters were supplied to Stripe's API
                            $response['error'] = true;
                            $response['message'] = $e->getMessage();
                        }
                        if(property_exists($product, 'deleted')){
                            if($product->deleted==1)
                                $new_product="update";
                        }else{
                            $new_product="avaliable";
                        }
                    }
                    if($new_product!="avaliable"){
            
                        try { 
                            $product = \Stripe\Product::create(array( 
                                "name" => $planName 
                            ));
                            $stripe_product_id	= $product->id;
                            if($new_product=="update"){
                                $data_update =  array( 
                                    'stripe_product_id' => $stripe_product_id, 
                                    'post_id' => $post_id,
                                    'stripe_mode' =>  $payment_mode,
                                );
                                $data_where = array('id' => $stripe_product_list[0]->id);
                                $wpdb->update($product_table_name , $data_update, $data_where);
                                
                            }else{
                                $wpdb->insert( 
                                    $product_table_name, 
                                    array(  
                                        'stripe_product_id' => $stripe_product_id, 
                                        'post_id' => $post_id,
                                        'stripe_mode' =>  $payment_mode,
                                    ) 
                                );
                            } 
                            $new_product="avaliable";                       
                        
                        }catch(Exception $e) { 
                            $response['error'] = true;
                            $response['message'] = $e->getMessage(); 
                        } 
                    }
                    if($new_product=="avaliable"){
                        $strip_plan_args=array(
                            "product_id" =>  $post_id,
                            "stripe_product_id" =>  $stripe_product_id,
                            "stripe_mode" =>  $payment_mode,
                        );
                        $stripe_plan_list=$this->get_stripe_plan_list($strip_plan_args);
                        if(!empty($stripe_plan_list)){
                            try{
                                $plan = \Stripe\Plan::retrieve(
                                    $stripe_plan_list[0]->stripe_price_id
                                );
                                $stripe_plan_id	= $plan->id;
                            }catch (Exception $e) {
                                // Invalid parameters were supplied to Stripe's API
                                $response['error'] = true;
                                $response['message'] = $e->getMessage();
                            }
                            if(property_exists($plan, 'deleted')){
                                if($plan->deleted==1)
                                    $new_plan="update";
                            }else{
                                $new_plan="avaliable";
                            }
                        }
                        if($new_plan!="avaliable"){
                    
                            // Create a plan 
                            try { 
                                $plan = \Stripe\Plan::create(array( 
                                    "product" => $stripe_product_id, 
                                    "amount" => $amount_cents, 
                                    "currency" => "usd", 
                                    "interval" => "month", 
                                ));
                                $stripe_plan_id	= $plan->id;
                                $stripe_plan_amount	= $plan->amount;
                                if($new_plan=="update"){
                                    $data_update =  array( 
                                        'stripe_product_id' => $stripe_product_id, 
                                        'stripe_price_id' => $stripe_plan_id,
                                        'price_value' => ($stripe_plan_amount/100),
                                        'product_id' => $post_id,
                                        'stripe_mode' =>  $payment_mode,
                                    );
                                    $data_where = array('id' => $stripe_plan_list[0]->id);
                                    $wpdb->update($product_price_table_name , $data_update, $data_where);
                                    
                                }else{
                                    $wpdb->insert( 
                                        $product_price_table_name, 
                                        array(  
                                            'stripe_product_id' => $stripe_product_id, 
                                            'stripe_price_id' => $stripe_plan_id,
                                            'price_value' => ($stripe_plan_amount/100),
                                            'product_id' => $post_id,
                                            'stripe_mode' =>  $payment_mode,
                                        ) 
                                    );
                                }                       
                                $new_plan="avaliable";
                                
                                
                            }catch(Exception $e) { 
                                $response['error'] = true;
                                $response['message'] = $e->getMessage(); 
                            } 
                        }
                    }
                    if($new_plan=="avaliable"){
                        try { 
                            $subscription = \Stripe\Subscription::create([
                                'customer' => $customer->id,
                                'items' => array( 
                                    array( 
                                        "price" => $plan->id, 
                                    ), 
                                ), 
                            ]); 
                            
                            $id = $subscription->id;
                            $subscription_id = $subscription->id;
                            $amount 	= $subscription->plan->amount;
                            $currency 	= $subscription->plan->currency;                    
                            $status 	= $subscription->status;
                            $plan_interval=$subscription->plan->interval;
                            $plan_interval_count=$subscription->plan->interval_count;
                            $plan_period_start=$subscription->current_period_start;
                            $plan_period_end=$subscription->current_period_end;
                            
                            $result = "succeeded";

                            $wpdb->insert( 
                                $subscription_table_name, 
                                array( 
                                    'user_id' => $customer_user_id,
                                    'stripe_subscription_id' =>  $subscription_id,
                                    'stripe_customer_id' => $customer->id, 
                                    'stripe_plan_id' => $plan->id,
                                    'plan_amount' => ($amount/100), 
                                    'plan_amount_currency' => $currency,
                                    'plan_interval' => $plan_interval,
                                    'plan_period_start' => date('Y-m-d H:i:s', $plan_period_start),
                                    'plan_period_end' => date('Y-m-d H:i:s', $plan_period_end),
                                    'plan_interval_count' => $plan_interval_count,
                                    'payment_status' => $status,
                                    'stripe_mode' =>  $payment_mode,
                                ) 
                            );

                            $wpdb->insert( 
                                $order_table_name, 
                                array( 
                                    'user_id' => $customer_user_id,
                                    'txn_id' => $id, 
                                    'paid_amount' => ($amount/100), 
                                    'paid_amount_currency' => $currency,
                                    'project_name' => $planName,
                                    'payment_type' => $_POST['plantype'],
                                    'payment_status' => $status,
                                    'stripe_mode' =>  $payment_mode,
                                ) 
                            );
                            $lastid=$wpdb->insert_id;
                            $mail_info_data=array(
                                'transcation_id' => $id,
                                'payment_id' => $lastid,
                                'customer_fullname' => $full_name,
                                'customer_fname' => $name_first,
                                'customer_lname' => $name_last,
                                'customer_email' => $email,
                                'donation_project' => $planName,
                                'donation_amount' => ($amount/100),
                                'paid_amount_currency' => $currency,
                                'donation_plan' => $_POST['plantype'],
    
                            );
                            $response['mail_status'] = $this->wpcd_donation_success_mail($mail_info_data);

                            $response['success'] = true;
                            //$response['message'] = "Thanks for your payment. <br/> Your Transaction Id: ".$id;
                            $response['message'] = "Thanks for your payment.";
                            
                        
                        }catch(Exception $e) { 
                            $response['error'] = true;
                            $response['message'] = $e->getMessage(); 
                        } 
                    }
                }
        
            }
            
                
        } 

        wp_send_json($response);
        die();

    }
    function get_stripe_plan_list($args=array()){
        global $wpdb;
        $table_name = $wpdb->prefix . 'donation_product_price_list';
        if($wpdb->get_var( "show tables like '$table_name'" ) ==  $table_name) 
        {
            $sql_con="";
            $sql_colum="";
            if(!empty($args)){
                if(isset($args['price_value'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " price_value = ".$args['price_value'];
                }
                if(isset($args['product_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " product_id = ".$args['product_id'];
                }
                if(isset($args['stripe_product_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " stripe_product_id = '".$args['stripe_product_id']."'";
                }
                if(isset($args['stripe_price_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " stripe_price_id = '".$args['stripe_price_id']."'";
                }
                if(isset($args['stripe_mode'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    
                    }
                    $sql_con.= " stripe_mode = '" .$args['stripe_mode']. "'";
                }                
                if(isset($args['order'])){
                    $order_by="id";
                    if(isset($args['order_by'])){
                        $order_by=$args['order_by'];
                    }
                    $sql_con.= " ORDER BY ".$order_by." ".$args['order'];
                }
                if(isset($args['distinct'])){
                    $sql_colum.=" DISTINCT ".$args['distinct'];
                }
                if(isset($args['filter_columns'])){
                    $sql_colum.=" ".implode(',', $args['filter_columns']);
                }
            }
            if($sql_con){
                $sql_con=" WHERE ".$sql_con; 
            }
        
            if($sql_colum==""){
                $sql_colum="*";
            }
            
            
            
        
            $sql = "SELECT $sql_colum FROM $table_name $sql_con";
            
            $row = $wpdb->get_results($sql); 
        
            return $row;
        }else{
            return ;
        }
    
    }
    function get_stripe_product_list($args=array()){
        global $wpdb;
        $table_name = $wpdb->prefix . 'donation_product_list';
        if($wpdb->get_var( "show tables like '$table_name'" ) ==  $table_name) 
        {
            $sql_con="";
            $sql_colum="";
            if(!empty($args)){
                if(isset($args['post_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " post_id = ".$args['post_id'];
                }
                if(isset($args['stripe_product_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " stripe_product_id = '".$args['stripe_product_id']."'";
                }
                if(isset($args['stripe_mode'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    
                    }
                    $sql_con.= " stripe_mode = '" .$args['stripe_mode']. "'";
                }
                
                if(isset($args['order'])){
                    $order_by="id";
                    if(isset($args['order_by'])){
                        $order_by=$args['order_by'];
                    }
                    $sql_con.= " ORDER BY ".$order_by." ".$args['order'];
                }
                if(isset($args['distinct'])){
                    $sql_colum.=" DISTINCT ".$args['distinct'];
                }
                if(isset($args['filter_columns'])){
                    $sql_colum.=" ".implode(',', $args['filter_columns']);
                }
            }
            if($sql_con){
                $sql_con=" WHERE ".$sql_con; 
            }
        
            if($sql_colum==""){
                $sql_colum="*";
            }
            
            
            
        
            $sql = "SELECT $sql_colum FROM $table_name $sql_con";
            
            $row = $wpdb->get_results($sql); 
        
            return $row;
        }else{
            return ;
        }
    
    }
    function get_stripe_customer_list($args=array()){
        global $wpdb;
        $table_name = $wpdb->prefix . 'donation_customer_list';
        if($wpdb->get_var( "show tables like '$table_name'" ) ==  $table_name) 
        {
            $sql_con="";
            $sql_colum="";
            if(!empty($args)){
                if(isset($args['email_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " email_id = '".$args['email_id']."'";
                }
                if(isset($args['stripe_customer_id'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    }
                    $sql_con.= " stripe_customer_id = '".$args['stripe_customer_id']."'";
                }
                if(isset($args['stripe_mode'])){
                    if($sql_con!=""){
                        if(isset($args['compare'])){
                            $sql_con.=" ".trim($args['compare'])." "; 
                        }else{
                            $sql_con.=" and "; 
                        }
                    
                    }
                    $sql_con.= " stripe_mode = '" .$args['stripe_mode']. "'";
                }
                
                if(isset($args['order'])){
                    $order_by="id";
                    if(isset($args['order_by'])){
                        $order_by=$args['order_by'];
                    }
                    $sql_con.= " ORDER BY ".$order_by." ".$args['order'];
                }
                if(isset($args['distinct'])){
                    $sql_colum.=" DISTINCT ".$args['distinct'];
                }
                if(isset($args['filter_columns'])){
                    $sql_colum.=" ".implode(',', $args['filter_columns']);
                }
            }
            if($sql_con){
                $sql_con=" WHERE ".$sql_con; 
            }
        
            if($sql_colum==""){
                $sql_colum="*";
            }
            
            
            
        
            $sql = "SELECT $sql_colum FROM $table_name $sql_con";
            
            $row = $wpdb->get_results($sql); 
        
            return $row;
        }else{
            return ;
        }
    
    }

   
}
